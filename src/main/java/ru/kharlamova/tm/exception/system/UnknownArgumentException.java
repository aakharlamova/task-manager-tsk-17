package ru.kharlamova.tm.exception.system;

import ru.kharlamova.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(final String argument) {
        super("Error! Unknown argument ``" + argument + "``.");
    }

}
