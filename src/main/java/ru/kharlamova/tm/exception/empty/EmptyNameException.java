package ru.kharlamova.tm.exception.empty;

import ru.kharlamova.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty.");
    }

}
