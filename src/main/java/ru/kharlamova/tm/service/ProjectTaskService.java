package ru.kharlamova.tm.service;

import ru.kharlamova.tm.api.repository.IProjectRepository;
import ru.kharlamova.tm.api.service.IProjectTaskService;
import ru.kharlamova.tm.api.repository.ITaskRepository;
import ru.kharlamova.tm.exception.empty.EmptyIdException;
import ru.kharlamova.tm.exception.entity.TaskNotFoundException;
import ru.kharlamova.tm.model.Project;
import ru.kharlamova.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private ITaskRepository taskRepository;

    private IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task bindTaskByProject(String taskId, String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.bindTaskByProject(taskId, projectId);
    }

    @Override
    public Task unbindTaskFromProject(String taskId) {
        if (taskId == null || taskId.isEmpty())  throw new EmptyIdException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.unbindTaskFromProject(taskId);
    }

    @Override
    public Project removeProjectById(String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeOneById(projectId);
    }

}
