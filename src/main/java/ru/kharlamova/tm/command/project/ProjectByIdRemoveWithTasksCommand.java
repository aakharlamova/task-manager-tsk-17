package ru.kharlamova.tm.command.project;

import ru.kharlamova.tm.command.AbstractProjectCommand;
import ru.kharlamova.tm.exception.entity.ProjectNotFoundException;
import ru.kharlamova.tm.model.Project;
import ru.kharlamova.tm.util.TerminalUtil;

public class ProjectByIdRemoveWithTasksCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-id-with-tasks";
    }

    @Override
    public String description() {
        return "Delete project with all its tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("[ENTER ID:]");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectTaskService().removeProjectById(projectId);
        if (project == null) throw new ProjectNotFoundException();
    }

}
