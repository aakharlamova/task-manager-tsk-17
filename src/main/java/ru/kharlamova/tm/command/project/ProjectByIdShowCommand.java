package ru.kharlamova.tm.command.project;

import ru.kharlamova.tm.command.AbstractProjectCommand;
import ru.kharlamova.tm.exception.entity.ProjectNotFoundException;
import ru.kharlamova.tm.model.Project;
import ru.kharlamova.tm.util.TerminalUtil;

public class ProjectByIdShowCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-view-by-id";
    }

    @Override
    public String description() {
        return "Show a project by id.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
